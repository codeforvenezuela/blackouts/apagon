const fetch = require('node-fetch');

async function sendToAngostura(data) {
  console.log('sending to angostura');
  let postBody = {
    type : "blackouts",
    version: "1",
    payload: Buffer.from(JSON.stringify(data)).toString('base64')
  };
  console.log(JSON.stringify(postBody));

  return fetch('https://us-central1-event-pipeline.cloudfunctions.net/dev-angosturagate', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(postBody)
  }).then(response => {
    return true; //until we get a valid json response
  }).catch(err => {console.log(err);});
}

function validatePostData(data) {
  if(data.hasOwnProperty('location')) {
    if(!data.location.hasOwnProperty('lat')) {
      throw Error('`lat` property missing from location');
    }
    if(!data.location.hasOwnProperty('lng')) {
      throw Error('`lat` property missing from location');
    }
  } else {
    throw Error('`location` property is required');
  }

  if(!data.hasOwnProperty('type')) {
    throw Error('`type` property is required');
  }
  if(!data.hasOwnProperty('source')) {
    throw Error('`source` property is required');
  }

  return true;
}

function transformGeoData({ geoDataType="Point", data }) {
  const transformedData = data;
  const { lat, lng } = data.location;

  transformedData.location = {
    type: geoDataType,
    coordinates: [ lng, lat ]
  }

  return transformedData;
}

module.exports = {
  validatePostData,
  transformGeoData,
  sendToAngostura
}
