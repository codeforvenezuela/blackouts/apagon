const express = require('express');
const path = require('path');
const fetch = require('node-fetch');
//const {PubSub} = require('@google-cloud/pubsub');

const apagonesCollection = 'apagon';
const lugaresCollection = 'lugares';
const router = express.Router();
const {
  getApagonesDBClient,
  validatePostData,
  transformGeoData,
  sendToAngostura
} = require('../lib');

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('getting index -',__dirname);
  res.sendFile(path.resolve('client', 'dist','client', 'index.html'));
});

// API routes
router.get('/apagon', async (req, res, next) => {
  const dbClient = await getApagonesDBClient();
  const results = await dbClient.find(apagonesCollection);

  res.json({ data: results });
});

router.post('/apagon', async (req, res, next) => {
  const dbClient = await getApagonesDBClient();
  const data = req.body;
  try {
    validatePostData(data);
    const transformedData = transformGeoData({ data } );
    const results = await dbClient.insert(apagonesCollection, transformedData);
    res.json({ data: results });
  } catch(error) {
    console.error(error);
  }
});

router.get('/apagon/near', async (req, res, next) => {
  const { lat, lng, max_distance, min_distance } = req.query;
  const dbClient = await getApagonesDBClient();

  const query = {
    location: {
      $near: {
        $geometry: {
          type: "Point",
          coordinates: [parseFloat(lng), parseFloat(lat)]
        },
        $maxDistance: parseFloat(max_distance),
        $minDistance: parseFloat(min_distance)
      }
    }
  };
  try {
    const results = await dbClient.find(apagonesCollection, query);
    res.json(results);
  }catch(error) {
    throw error;
  }
})
router.get('/apagon/:id', async (req, res, next) => {
  const dbClient = await getApagonesDBClient();
  const results = await dbClient.find(apagonesCollection ,{
    _id: req.params
  });

  res.json(results);
});

// API routes
router.get('/lugar', async (req, res, next) => {
  const dbClient = await getApagonesDBClient();
  const results = await dbClient.find(lugaresCollection);

  res.json({ data: results });
});

router.get('/lugar/near', async (req, res, next) => {
  const { lat, lng, max_distance, min_distance } = req.query;
  const dbClient = await getApagonesDBClient();

  const query = {
    location: {
      $near: {
        $geometry: {
          type: "Point",
          coordinates: [parseFloat(lng), parseFloat(lat)]
        },
        $maxDistance: parseFloat(max_distance),
        $minDistance: parseFloat(min_distance)
      }
    }
  };
  try {
    res.json(await dbClient.find(lugaresCollection, query));
  }catch(error) {
    throw error;
  }
});


router.post('/lugar', async (req, res, next) => {
  const dbClient = await getApagonesDBClient();
  const data = req.body;
  try {
    validatePostData(data);
    const transformedData = transformGeoData({ data } );
    const results = await dbClient.insert(lugaresCollection, transformedData);
    res.json({ data: results });
  } catch (error) {
    console.error(error);
  }
});

//https://706fbfbd.ngrok.io/bot
router.post('/bot', async (req, res, next) => {
  // const dbClient = await getApagonesDBClient();
  const data = req.body;
  const botId = '956660225:AAGkL20tVJVhdXYFLQ7QUYAbuGHYToje8Go';

  try {
    let params = data.queryResult.parameters;
    let requesterIntent = data.originalDetectIntentRequest.payload.from;

    // console.log('params');
    // console.log(params);
    // console.log('requester');
    // console.log(requesterIntent);
 
    if(sendToAngostura){
      const angRes = await sendToAngostura(params);
      console.log('angRes '+angRes);
    }
    
    let botRes = 'Tu numero de incidente es 1234';
    if(requesterIntent){ //telegram?
      fetch('https://api.telegram.org/bot'+botId+'/sendMessage?chat_id='+requesterIntent.id+'&text='+encodeURI(botRes), {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
      }).then(response => {
        //console.log(response.json());
        return true;
      }).catch(err => {console.log(err);});

    }

    
    // const dbClient = await getApagonesDBClient();
    // //const data = req.body;
    // try {
    //   //validatePostData(data);
    //   //const transformedData = transformGeoData({ params } );
    //   const results = await dbClient.insert(apagonesCollection, params);
    //   res.json({ data: results });
    // } catch(error) {
    //   console.error(error);
    // }
    
    
    res.json({ data: 'results' });
  } catch(error) {
    console.error(error);
  }
});


module.exports = router;

