if (process.env.NODE_ENV === 'local') {
  require('dotenv').config();
}
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
var cors = require('cors');
const RateLimit = require('express-rate-limit');
const helmet = require('helmet');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const Telegraf = require('telegraf');

const {PubSub} = require('@google-cloud/pubsub');


const app = express();

app.use(cors());
app.set('port', process.env.PORT || 3000);

const limiter = new RateLimit({
  windowMs: parseInt(process.env.WINDOW_MS || 60000, 10),
  max: parseInt(process.env.MAX_IP_REQUESTS || 5000, 10),
  delayMs: parseInt(process.env.DELAY_MS || 0, 10),
  headers: true
});




// const bot = new Telegraf('913025940:AAHJGjUNxY3LvM22F1wTf6k-f3STL7Rfb8M');
// bot.start((ctx) => ctx.reply('Welcome!'));
// bot.help((ctx) => ctx.reply('Send me a sticker'));
// bot.on('sticker', (ctx) => ctx.reply('👍'));
// bot.hears('hi', (ctx) => ctx.reply('Hey there'));
// bot.launch();


app.use(limiter);
app.use(helmet());
app.use(express.static('client'))
app.use(function (req, res, next) {
  res.setHeader('Server', '');
  res.setHeader('Via', '');
  next();
});
app.use(helmet.hsts({
  maxAge: 31536000,
  preload: false
}));

// view engine setup

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);


// Creates a client
const pubsub = new PubSub();

/**
 * TODO(developer): Uncomment the following lines to run the sample.
 */
// const topicName = 'my-topic';
// const data = JSON.stringify({ foo: 'bar' });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.sendStatus(err.status || 500);
});

module.exports = app;
