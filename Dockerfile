FROM node:8

EXPOSE 3000
COPY . /home/node/app

WORKDIR /home/node/app
# ENTRYPOINT [ "/bin/bash" ]

# Install app dependencies
COPY package.json /home/node/app
RUN npm install

CMD [ "/bin/bash", "-c", "npm start" ]
